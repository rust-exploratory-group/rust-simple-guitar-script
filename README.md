# Rust Simple Guitar Script

A simple Rust guitar script that allows for the user to unleash sick guitar solo's in the midst of the battlefield.

### Prerequisites

Visual Studio 2017
.NET Framework 4.7.2
Rust (playrust.com)

### Installing

Edit keybinds, build, and enjoy!

```



## Deployment

Run as administrator to allow adequate permissions for the build to run. 

## Built With

* [Codedom](https://docs.microsoft.com/en-us/dotnet/api/system.codedom) - Dynamic Compilation

## Authors

* **Daddy** - *Initial work* - [BaseBitch](https://github.com/dotnet/corefx)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Shout out to my good friend hJune who showed me the framework for the violin!
Also thanks to Lifestomper, ShackyHD, and my boy Jamal.
