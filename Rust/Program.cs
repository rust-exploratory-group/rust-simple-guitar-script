﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Source_Randomizer;

namespace Rust
{
	internal static class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			//var baseSource = Resources.RustCompiled;
			string baseSource = Properties.Resources.Rust;
			
			//var tmpIcon = GenerateIcon(16, 16);
			//var iconPath = Path.Combine(Application.StartupPath, "executableIcon.ico");
			//SaveAsIcon(tmpIcon, iconPath);
			//tmpIcon.Dispose();

			var randomName = "generated_" + GenerateRandomVariable(8, 13) + "_" +
			                 DateTime.Now.ToLongTimeString().Replace(" ", string.Empty).Replace(":", "-") + ".exe";

			int result = Codedom.BuildExecutable("", ModifySource(baseSource), Path.Combine(Application.StartupPath, randomName));
			if (result == 1)
			{
				//bool res = Enigma(Path.Combine(Application.StartupPath, randomName));
				Process.Start("file://" + Application.StartupPath);
			}
		}

		public static string XOR_STR(string toCrypt, byte xorByte)
		{
			var inSb = new StringBuilder(toCrypt);
			var outSb = new StringBuilder(toCrypt.Length);

			for (int i = 0; i < toCrypt.Length; i++)
			{
				char c = inSb[i];
				c = (char)(c ^ xorByte);
				outSb.Append(c);
			}

			return outSb.ToString();
		}

		public static string ModifySource(string source)
		{
			byte xorByte = (byte)new Random(Guid.NewGuid().GetHashCode()).Next(1, 255);
			Random r = new Random(Guid.NewGuid().GetHashCode());

			MatchCollection allMatchResults = null;
			var regexObj = new Regex("%.*?%");
			allMatchResults = regexObj.Matches(source);
			var variableNamesUsed = new List<string>();

			if (allMatchResults.Count > 0)
			{
				var R = new Random(Guid.NewGuid().GetHashCode());
				foreach (Match regexMatch in allMatchResults)
				{
					switch (regexMatch.Value)
					{
						case "%asm_guid%":
							source = source.Replace("%asm_guid%", Guid.NewGuid().ToString());
							break;
						case "%asm_ver1%":
							var randomVersion = R.Next(0, 9) + "." + R.Next(0, 9) + "." + R.Next(4, 72) + "." + R.Next(1, 98);
							source = source.Replace("%asm_ver1%", randomVersion);
							break;
						case "%asm_ver2%":
							var randomVersion1 = R.Next(0, 9) + "." + R.Next(0, 9) + "." + R.Next(0, 99) + "." + R.Next(4, 62);
							source = source.Replace("%asm_ver2%", randomVersion1);
							break;
					}
				}
			}

			/*
			string[] varNames = Codedom.ExtractVariableNamesFromSource(source);
			StringBuilder sb = new StringBuilder(source);
			foreach (var varName in varNames)
			{
				if (varName.Length < 3) continue;
				switch (varName.ToLower())
				{
					case "keys":
					case "key":
					case "value":
					case "sleep":
					case "name":
					case "file":
					case "args":
					case "success":
					case "sectionname":
						continue;
					default:
						// Replace each parsed variable name with a randomly generated one
						//source = source.Replace(varName, Program.GenerateRandomVariable(r, 230, 231));
						sb.Replace(varName, GenerateRandomVariable(r, varName.Length, varName.Length));
						break;
				}
			}

			source = sb.ToString();

			var methodNames = Codedom.ExtractMethodNamesFromSource(source);
			foreach (var methodName in methodNames)
			{
				switch (methodName.ToLower())
				{
					case "xor_str":
					case "main":
					case "registerhotkey":
					case "unregisterhotkey":
					case "registerhotkeyinternal":
					case "wndproc":
					case "setvisiblecore":
					case "getasynckeystate":
					case "mouse_event":
					case "keybd_event":
					case "getdc":
					case "releasedc":
					case "keypress":
					case "parse":
						continue;
					default:
						// replace shit
						source = source.Replace(methodName, GenerateRandomVariable(r, methodName.Length, methodName.Length));
						break;
				}
			}

			Clipboard.SetText(sb.ToString());
			*/

			if (Randomizer.OpenDocument(source))
			{
				Randomizer.LineOrderRandomizer();

				string sourceToReturn = Randomizer.DocumentToString();
				MatchCollection allMatchResults4 = null;
				var regexObj4 = new Regex(@""".*?""");
				allMatchResults4 = regexObj4.Matches(sourceToReturn.Substring(sourceToReturn.IndexOf("namespace")));

				int stringsSkipped = 0;
				int stringsWorked = 1;

				foreach (Match m in allMatchResults4)
				{
					if (m.Value.Split('"')[1].Split('"')[0] == "") continue;
					if (m.Value.Split('"')[1].Split('"')[0].StartsWith("#"))
					{
						// replace string starting with # with same string but without #
						sourceToReturn = sourceToReturn.Replace(m.Value, '"' + m.Value.Split('"')[1].Split('"')[0].Substring(1) + '"');
						stringsSkipped++;
					}
					else
					{
						string encoded = XOR_STR(m.Value.Split('\"')[1].Split('\"')[0], xorByte);
						sourceToReturn = sourceToReturn.Replace(m.Value, "XOR_STR(@\"" + encoded + "\", (byte)" + xorByte + ")");
						stringsWorked++;
					}
				}

				sourceToReturn = sourceToReturn.Replace("XOR_STR", GenerateRandomVariable(200, 231));

				Clipboard.SetText(sourceToReturn);
				return sourceToReturn;
			}

			MatchCollection allMatchResults3 = null;
			var regexObj3 = new Regex(@""".*?""");
			allMatchResults3 = regexObj3.Matches(source.Substring(source.IndexOf("namespace")));

			foreach (Match m in allMatchResults3)
			{
				if (m.Value.Split('"')[1].Split('"')[0] == "") continue;
				if (m.Value.Split('"')[1].Split('"')[0].StartsWith("#"))
				{

					// replace string starting with # with same string but without #
					source = source.Replace(m.Value, '"' + m.Value.Split('"')[1].Split('"')[0].Substring(1) + '"');
				}
				else
				{
					string encoded = XOR_STR(m.Value.Split('\"')[1].Split('\"')[0], xorByte);
					source = source.Replace(m.Value, "XOR_STR(@\"" + encoded + "\", (byte)" + xorByte + ")");
				}
			}

			source = source.Replace("XOR_STR", GenerateRandomVariable(200, 231));
			return source;
		}

		public static bool ContainsLetters(string str)
		{
			foreach (char c in str)
			{
				if (char.IsLetter(c)) return true;
			}
			return false;
		}

		public static Bitmap GenerateIcon(int height, int width)
		{
			if (height < 1 || width < 1)
			{
				height = 32;
				width = 32;
			}

			if (height > 128 || width > 128)
			{
				height = 128;
				width = 128;
			}

			var bmp = new Bitmap(height, width);

			//random number
			var rand = new Random();

			//create random pixels
			for (var y = 0; y < height; y++)
			{
				for (var x = 0; x < width; x++)
				{
					//generate random ARGB value
					var a = rand.Next(256);
					var r = rand.Next(256);
					var g = rand.Next(256);
					var b = rand.Next(256);

					//set ARGB value
					bmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
				}
			}

			return bmp;
		}

		public static void SaveAsIcon(Bitmap SourceBitmap, string FilePath)
		{
			try
			{
				var FS = new FileStream(FilePath, FileMode.Create);
				// ICO header
				FS.WriteByte(0);
				FS.WriteByte(0);
				FS.WriteByte(1);
				FS.WriteByte(0);
				FS.WriteByte(1);
				FS.WriteByte(0);

				// Image size
				FS.WriteByte((byte) SourceBitmap.Width);
				FS.WriteByte((byte) SourceBitmap.Height);
				// Palette
				FS.WriteByte(0);
				// Reserved
				FS.WriteByte(0);
				// Number of color planes
				FS.WriteByte(0);
				FS.WriteByte(0);
				// Bits per pixel
				FS.WriteByte(32);
				FS.WriteByte(0);

				// Data size, will be written after the data
				FS.WriteByte(0);
				FS.WriteByte(0);
				FS.WriteByte(0);
				FS.WriteByte(0);

				// Offset to image data, fixed at 22
				FS.WriteByte(22);
				FS.WriteByte(0);
				FS.WriteByte(0);
				FS.WriteByte(0);

				// Writing actual data
				SourceBitmap.Save(FS, ImageFormat.Png);

				// Getting data length (file length minus header)
				var Len = FS.Length - 22;

				// Write it in the correct place
				FS.Seek(14, SeekOrigin.Begin);
				FS.WriteByte((byte) Len);
				FS.WriteByte((byte) (Len >> 8));

				FS.Close();
			}
			catch
			{
				MessageBox.Show("Or better yet, fix this issue :)", "SaveAsIcon failed, you can ignore this!");
			}
			
		}

		public static string GenerateRandomVariable(int lengthLowerBound = 32, int lengthUpperBound = 231)
		{
			Random rand = new Random(Guid.NewGuid().GetHashCode());
			var chars = new char[52];
			var maxSize = rand.Next(lengthLowerBound, lengthUpperBound);

			chars =
				"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
			var data = new byte[1];
			using (var crypto = new RNGCryptoServiceProvider())
			{
				crypto.GetNonZeroBytes(data);
				data = new byte[maxSize];
				crypto.GetNonZeroBytes(data);
			}
			var result = new StringBuilder(maxSize);
			foreach (var b in data)
				result.Append(chars[b % chars.Length]);

			return result.ToString();
		}

		public static void PumpExecutable(string executableLocation, int numKbytes)
		{
			if (!File.Exists(executableLocation)) return;
			var f = File.OpenWrite(executableLocation);
			long bytesWritten = 0;
			var r = new Random(Guid.NewGuid().GetHashCode());
			while (bytesWritten < numKbytes * 1024)
			{
				f.WriteByte(0);
				bytesWritten++;
			}

			f.Close();
		}
	}
}