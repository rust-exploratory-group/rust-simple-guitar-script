[WEAPON_AK]
Name=AK
HotKeyInt=113
ShotTiming=134
Mode=1
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=5
MovementMultiplier=4
RecoilTable={{-36, 40}{5, 48}{-59, 48}{-49, 48}{3, 33}{20, 33}{25, 28}{45, 24}{43, 16}{32, 13}{82, 18}{8, 22}{43, 24}{-32, 29}{-25, 33}{-40, 33}{-35, 33}{-32, 29}{-43, 22}{-42, 20}{-42, 17}{-55, 17}{-25, 17}{15, 17}{20, 20}{35, 27}{50, 27}{62, 27}{40, 26}}

[WEAPON_LR300]
Name=LR300
HotKeyInt=116
ShotTiming=120
Mode=1
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=5
MovementMultiplier=2
RecoilTable={{-3, 50}{-3, 55}{-20, 75}{-5, 55}{-35, 45}{-20, 25}{-25, 25}{-5, 15}{30, 25}{30, 25}{60, 25}{20, 35}{20, 5}{20, 5}{-30, 5}{-30, 5}{-20, 5}{-30, 5}{-30, 5}{-30, 0}{-20, 0}{-20, 0}{-20, 0}{-30, 0}{20, 0}{30, 0}{40, 0}{50, 30}}

[WEAPON_M249]
Name=M249
HotKeyInt=115
ShotTiming=120
Mode=1
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=5
MovementMultiplier=2
RecoilTable={{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}}

[WEAPON_Thomson]
Name=Thomson
HotKeyInt=114
ShotTiming=130
Mode=1
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=5
MovementMultiplier=2.5
RecoilTable={{-25, 50}{0, 45}{0, 50}{0, 50}{30, 45}{0, 40}{0, 40}{0, 40}{0, 20}{-20, 20}{0, 20}{-30, 20}{0, 20}{0, 20}{50, 20}{20, 10}{10, 10}{10, 15}}

[WEAPON_CUSTOM]
Name=CUSTOM SMG
HotKeyInt=118
ShotTiming=100
Mode=1
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=5
MovementMultiplier=2
RecoilTable={{-25, 50}{0, 45}{0, 50}{0, 50}{30, 45}{0, 40}{0, 40}{0, 40}{0, 20}{-20, 20}{0, 20}{-30, 20}{0, 20}{0, 20}{50, 20}{20, 10}{10, 10}{10, 15}}

[WEAPON_SAR]
Name=SAR
HotKeyInt=117
ShotTiming=175
Mode=2
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=1
MovementMultiplier=13
RecoilTable={{0, 60}{0, 60}{0, 60}{0, 60}{0, 60}{0, 75}{0, 95}{0, 95}{0, 55}{0, 75}{0, 75}{0, 75}{0, 55}{0, 75}{0, 45}}

[WEAPON_M92]
Name=M92
HotKeyInt=119
ShotTiming=100
Mode=2
ShouldRandomize=True
RandomizeLowerBound=0
RandomizeUpperBound=3
ShouldSmooth=True
SmoothValue=1
MovementMultiplier=13
RecoilTable={{0, 12}}