//randstart
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Security.Principal;
using System.Text;
//randend


namespace Rust.Resources
{
	internal static class ScriptName
	{
		[Flags]
		public enum KeyModifiers
		{
			//randstart
			None = 0,
			Alt = 1,
			Control = 2,
			Shift = 4,
			Windows = 8,
			NoRepeat = 0x4000,
			//randend
		}

		public static bool IsElevated
		{
			get
			{
				bool r = new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);
				return r;
			}
			set { IsElevated = value; }
		}

		//randstart
		public static bool IsEnabled;
		public static bool IsDrawingsEnabled;
		public static KeyModifiers ModifierKeyFromSettings = KeyModifiers.Control;
		public static Point centerPoint = new Point(Screen.PrimaryScreen.Bounds.Width / 2 + Drawings.crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + Drawings.crossYLocationOffset);
		//randend

		private static void HotKeyManager_HotKeyPressed(object sender, HotKeyEventArgs eventargs)
		{
			if (eventargs.Modifiers != ModifierKeyFromSettings) return;
			switch (eventargs.Key)
			{
				case Keys.Tab:

					//randstart
					IsEnabled = !IsEnabled;
					Console.Title = "Current Weapon: " + Weapons.CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
					//randend

					switch (IsEnabled)
					{
						case true:
							Logger.Log(Logger.MessageType.INFO, "Slash has stepped on to the stage!");
							break;
						case false:
							Logger.Log(Logger.MessageType.INFO, "Slash's solo is over!");
							break;
					}
					break;
				case Keys.Divide:
					IsDrawingsEnabled = !IsDrawingsEnabled;
					switch (IsDrawingsEnabled)
					{
						case true:
							if (!Drawings.IsDrawerInitiated) Drawings.TryInit();
							Logger.Log(Logger.MessageType.INFO, "Drawing has been enabled!");
							break;
						case false:
							if (Drawings.IsDrawerInitiated) Drawings.TryDisable();
							Logger.Log(Logger.MessageType.INFO, "Drawing has been disabled!");
							break;
					}
					break;
				case Keys.Multiply:
					Drawings.ShouldDrawCrosshair = !Drawings.ShouldDrawCrosshair;
					switch (Drawings.ShouldDrawCrosshair)
					{
						case true:
							Logger.Log(Logger.MessageType.INFO, "Drawing of melodies has been enabled!");
							break;
						case false:
							Logger.Log(Logger.MessageType.INFO, "Drawing of melodies has been disabled!");
							break;
					}
					break;
				case Keys.Subtract:
					Drawings.ShouldDrawCurrentWeapon = !Drawings.ShouldDrawCurrentWeapon;
					switch (Drawings.ShouldDrawCurrentWeapon)
					{
						case true:
							Logger.Log(Logger.MessageType.INFO, "Drawing of current sheet notation has been enabled!");
							break;
						case false:
							Logger.Log(Logger.MessageType.INFO, "Drawing of current sheet notation has been disabled!");
							break;
					}
					break;
				case Keys.Left:
					Drawings.crossXLocationOffset--;
					centerPoint = new Point(Screen.PrimaryScreen.Bounds.Width / 2 + Drawings.crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + Drawings.crossYLocationOffset);
					break;
				case Keys.Right:
					Drawings.crossXLocationOffset++;
					centerPoint = new Point(Screen.PrimaryScreen.Bounds.Width / 2 + Drawings.crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + Drawings.crossYLocationOffset);
					break;
				case Keys.Up:
					Drawings.crossYLocationOffset--;
					centerPoint = new Point(Screen.PrimaryScreen.Bounds.Width / 2 + Drawings.crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + Drawings.crossYLocationOffset);
					break;
				case Keys.Down:
					Drawings.crossYLocationOffset++;
					centerPoint = new Point(Screen.PrimaryScreen.Bounds.Width / 2 + Drawings.crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + Drawings.crossYLocationOffset);
					break;
				case Keys.NumPad0:
					Weapons.ForceReloadSettings();
					break;
				/*case Keys.XButton1:
					// Cycle through weapons forward by 1
					Weapons.WeaponCycle();
					break;
				case Keys.XButton2:
					// Cycle through weapons backwards by 1
					Weapons.WeaponCycle(0);
					break;
				*/
				default:
					Weapons.ParsePressedKey(eventargs.Key);
					break;
			}
		}

		public static string XOR_STR(string toCrypt, byte xorByte)
		{
			var inSb = new StringBuilder(toCrypt);
			var outSb = new StringBuilder(toCrypt.Length);

			for (int i = 0; i < toCrypt.Length; i++)
			{
				char c = inSb[i];
				c = (char)(c ^ xorByte);
				outSb.Append(c);
			}

			return outSb.ToString();
		}

		public static KeyModifiers ReadModifierKeyFromSettings()
		{
			if (!File.Exists(Path.Combine(Application.StartupPath, "settings.ini"))) return KeyModifiers.Control;

			try
			{
				if (IniClass.OpenIni(Path.Combine(Application.StartupPath, "settings.ini")))
				{
					IniClass.Sections generalSection = IniClass.Reader.ExtractSection("GENERAL");
					IniClass.CloseIni();

					if (generalSection == null) return KeyModifiers.Control;
					
					 switch (Int32.Parse(generalSection.SectionParameters["ModifierKey"]))
					{
						case 0:
							return KeyModifiers.None;
						case 1:
							return KeyModifiers.Alt;
						case 2:
							return KeyModifiers.Control;
						case 3:
							return KeyModifiers.Shift;
						case 4:
							return KeyModifiers.Windows;
						default:
							return KeyModifiers.Control;
					}
				}

				return KeyModifiers.Control;
			}
			catch
			{
				return KeyModifiers.Control;
			}
		}

		private static void Main(string[] args)
		{
			if (!IsElevated)
			{
				MessageBox.Show("Please run the program as administrator!", "Missing Admin Privileges!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			bool shouldLoadFromSettings = true;
			if (!File.Exists(Path.Combine(Application.StartupPath, "settings.ini")))
			{
				try
				{
					using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Rust.Resources.Resources.resources")) // Used when compiling with Roslyn
					{
						if (resource == null)
						{
							Logger.Log(Logger.MessageType.WARNING, "'Resource.resx' is null, falling back to backup code ... [ERRCODE: 3]");

							using (var resourceBackup = Assembly.GetExecutingAssembly().GetManifestResourceStream("Resources.resx"))
							{
								if (resourceBackup == null)
								{
									Logger.Log(Logger.MessageType.ERROR, "Backup code failed to write template 'settings.ini' to disk, please contact the developer! [ERRCODE: 4]");
									shouldLoadFromSettings = false;
								}
								else
								{
									bool success = false;
									var resourceReader = new System.Resources.ResourceReader(resourceBackup);
									foreach (DictionaryEntry dItem in resourceReader)
									{
										if (dItem.Key.ToString() == "settings.ini")
										{
											File.WriteAllBytes(Path.Combine(Application.StartupPath, "settings.ini"), (byte[])dItem.Value);
											success = true;
										}
									}

									if (!success)
									{
										Logger.Log(Logger.MessageType.ERROR, "Failed writing template 'settings.ini' file from resources to disk! [ERRCODE: 5]");
										shouldLoadFromSettings = false;
									}
									else
									{
										Logger.Log(Logger.MessageType.INFO, "Backup code successfully wrote template 'settings.ini' to disk!");
									}
								}
							}
						}
						else
						{
							bool success = false;
							var resourceReader = new System.Resources.ResourceReader(resource);
							foreach (DictionaryEntry dItem in resourceReader)
							{
								if (dItem.Key.ToString() == "settings.ini")
								{
									File.WriteAllBytes(Path.Combine(Application.StartupPath, "settings.ini"), (byte[])dItem.Value);
									success = true;
								}
							}

							if (!success)
							{
								Logger.Log(Logger.MessageType.ERROR, "Failed writing template 'settings.ini' file from resources to disk! [ERRCODE: 2]");
								shouldLoadFromSettings = false;
							}
						}
					}
				}
				catch (Exception)
				{
					//randstart
					Logger.Log(Logger.MessageType.ERROR, "Failed writing template 'settings.ini' file from resources to disk! [ERRCODE: 1]");
					shouldLoadFromSettings = false;
					//randend
				}
			}

			//randstart
			var errorEncountered = false;
			var R = new Random(Guid.NewGuid().GetHashCode());
			var R1 = new Random(Guid.NewGuid().GetHashCode());
			string resp = "";
			//randend

			
			if (shouldLoadFromSettings)
			{
				Console.WriteLine("Do you want to load settings from the settings.ini file? (y/n)");
				resp = Console.ReadLine();
			}

			if (shouldLoadFromSettings && File.Exists(Path.Combine(Application.StartupPath, "settings.ini")) && resp[0].ToString().ToLower() == "y")
			{
				Weapons.InitializeWeapons(Path.Combine(Application.StartupPath, "settings.ini"));
				ModifierKeyFromSettings = ReadModifierKeyFromSettings();
			}
			else
			{
				Weapons.InitializeWeapons("");
			}

			if (Weapons.WeaponList.Count < 1) errorEncountered = true;
			if (!errorEncountered)
			{
				//randstart
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Tab, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Divide, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Multiply, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Subtract, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Left, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Right, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Up, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.Down, ModifierKeyFromSettings));
				HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(Keys.NumPad0, ModifierKeyFromSettings));
				//randend

				foreach (var wI in Weapons.WeaponList)
				{
					HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(wI.Hotkey, ModifierKeyFromSettings));
				}

				//randstart
				HotKeyManager.HotKeyPressed += HotKeyManager_HotKeyPressed;
				var thread = new Thread(DrawingThread);
				//randend

				thread.Start();

				Console.Title = "Current Instrument: " + Weapons.CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
			}
			while (!errorEncountered)
			{
				if (!IsEnabled || Weapons.CurrentWeapon == null)
				{
					Thread.Sleep(10);
					continue;
				}
				if (Mouse.IsKeyDown(Keys.LButton))
					switch (Weapons.CurrentWeapon.Mode)
					{
						case Weapons.FiringMode.Automatic:
							while (Mouse.IsKeyDown(Keys.LButton) && Mouse.IsKeyDown(Keys.RButton))
								for (var currentShotIndex = 0; currentShotIndex < Weapons.CurrentWeapon.RecoilTable.Length; currentShotIndex++)
								{
									if (!Mouse.IsKeyDown(Keys.LButton)) break;
									if (Weapons.CurrentWeapon.SmoothValue > 1 && Weapons.CurrentWeapon.ShouldSmooth)
									{
										for (var smoothIndex = 0; smoothIndex < Weapons.CurrentWeapon.SmoothValue; smoothIndex++)
										{
											if (!Mouse.IsKeyDown(Keys.LButton)) break;
											if (Weapons.CurrentWeapon.ShouldRandomize)
											{
												Mouse.RelativeMove(
													((int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][0] * Weapons.CurrentWeapon.MovementMultiplier, 0) + R.Next(Weapons.CurrentWeapon.RandomizeLowerBound, Weapons.CurrentWeapon.RandomizeUpperBound)) /
													Weapons.CurrentWeapon.SmoothValue,
													((int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][1] * Weapons.CurrentWeapon.MovementMultiplier, 0) + R.Next(Weapons.CurrentWeapon.RandomizeLowerBound, Weapons.CurrentWeapon.RandomizeUpperBound)) /
													Weapons.CurrentWeapon.SmoothValue);
												Thread.Sleep(Weapons.CurrentWeapon.ShotTiming / Weapons.CurrentWeapon.SmoothValue);
											}
											else
											{
												Mouse.RelativeMove((int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][0] * Weapons.CurrentWeapon.MovementMultiplier, 0) / Weapons.CurrentWeapon.SmoothValue,
													(int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][1] * Weapons.CurrentWeapon.MovementMultiplier, 0) / Weapons.CurrentWeapon.SmoothValue);
												Thread.Sleep(Weapons.CurrentWeapon.ShotTiming / Weapons.CurrentWeapon.SmoothValue);
											}
										}
									}
									else
									{
										if (Weapons.CurrentWeapon.ShouldRandomize)
										{
											Mouse.RelativeMove(
												(int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][0] * Weapons.CurrentWeapon.MovementMultiplier, 0) + R.Next(Weapons.CurrentWeapon.RandomizeLowerBound, Weapons.CurrentWeapon.RandomizeUpperBound),
												(int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][1] * Weapons.CurrentWeapon.MovementMultiplier, 0) + R.Next(Weapons.CurrentWeapon.RandomizeLowerBound, Weapons.CurrentWeapon.RandomizeUpperBound));
											Thread.Sleep(Weapons.CurrentWeapon.ShotTiming);
										}
										else
										{
											Mouse.RelativeMove(
												(int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][0] * Weapons.CurrentWeapon.MovementMultiplier, 0),
												(int)Math.Round(Weapons.CurrentWeapon.RecoilTable[currentShotIndex][1] * Weapons.CurrentWeapon.MovementMultiplier, 0));
											Thread.Sleep(Weapons.CurrentWeapon.ShotTiming);
										}
									}
								}
							break;
						case Weapons.FiringMode.SemiAutomatic:
							while (Mouse.IsKeyDown(Keys.LButton) && Mouse.IsKeyDown(Keys.RButton))
							{
								if (!Mouse.IsKeyDown(Keys.LButton) && !Mouse.IsKeyDown(Keys.RButton)) break;
								var rSleep = R1.Next(63, 98);

								switch (Weapons.CurrentWeapon.Name)
								{
									case "#SAR":
										Mouse.RelativeMove(0, (int)(12 * Weapons.CurrentWeapon.MovementMultiplier));
										break;
									case "#M92":
										Mouse.RelativeMove(0, (int)Math.Round(15 * Weapons.CurrentWeapon.MovementMultiplier, 0));
										break;
								}

								Mouse.KeyPress(0x22, rSleep);
								Thread.Sleep(Weapons.CurrentWeapon.ShotTiming - rSleep);
							}

							break;
						default:
							MessageBox.Show("FUCKED!");
							break;

					}
				Thread.Sleep(5);
			}
			Drawings.TryDisable();

			foreach (var hotkeyId in HotKeyManager.RegisteredKeys)
			{
				HotKeyManager.UnregisterHotKey(hotkeyId);
			}

			Logger.Log(Logger.MessageType.ERROR, "Fatal Error encountered somewhere, please restart the program!");
			Console.ReadLine();
		}

		private static void DrawingThread()
		{
			while (true)
			{
				if (IsDrawingsEnabled && Drawings.IsDrawerInitiated)
				{
					//randstart
					Drawings.DrawCrosshair();
					Drawings.DrawCurrentWeapon();
					//randend

					Thread.Sleep(5);
					continue;
				}
				Thread.Sleep(100);
			}
		}

		public static class HotKeyManager
		{
			//randstart
			public static List<int> RegisteredKeys = new List<int>();
			private static volatile MessageWindow _wnd;
			private static volatile IntPtr _hwnd;
			private static readonly ManualResetEvent _windowReadyEvent = new ManualResetEvent(false);
			private static int _id;
			//randend

			static HotKeyManager()
			{
				var messageLoop = new Thread(() => Application.Run(new MessageWindow())) { Name = "%randomstring_messageloop%", IsBackground = true };
				messageLoop.Start();
			}

			public static event EventHandler<HotKeyEventArgs> HotKeyPressed;

			public static int RegisterHotKey(Keys key_internal2, KeyModifiers modifiers_internal_2)
			{
				_windowReadyEvent.WaitOne();
				var id_internal_3 = Interlocked.Increment(ref _id);
				_wnd.Invoke(new RegisterHotKeyDelegate(RegisterHotKeyInternal), _hwnd, id_internal_3, (uint)modifiers_internal_2, (uint)key_internal2);
				return id_internal_3;
			}

			public static void UnregisterHotKey(int id_internal_1)
			{
				_wnd.Invoke(new UnRegisterHotKeyDelegate(UnRegisterHotKeyInternal), _hwnd, id_internal_1);
			}

			private static void RegisterHotKeyInternal(IntPtr hwnd_internal, int id_internal, uint modifiers_internal, uint key_internal)
			{
				RegisterHotKey(hwnd_internal, id_internal, modifiers_internal, key_internal);
			}

			private static void UnRegisterHotKeyInternal(IntPtr hwnd, int id_int)
			{
				UnregisterHotKey(_hwnd, id_int);
			}

			private static void OnHotKeyPressed(HotKeyEventArgs eventargs1)
			{
				if (HotKeyPressed != null)
				{
					HotKeyPressed(null, eventargs1);
				}
			}

			[DllImport("#user32", SetLastError = true)]
			private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

			[DllImport("#user32", SetLastError = true)]
			private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

			//randstart
			private delegate void RegisterHotKeyDelegate(IntPtr hwnd, int id, uint modifiers, uint key);
			private delegate void UnRegisterHotKeyDelegate(IntPtr hwnd, int id);
			//randend

			private class MessageWindow : Form
			{
				private const int WM_HOTKEY = 0x312;

				public MessageWindow()
				{
					//randstart
					_wnd = this;
					_hwnd = Handle;
					//randend

					_windowReadyEvent.Set();
				}

				protected override void WndProc(ref Message m)
				{
					if (m.Msg == WM_HOTKEY)
					{
						var eevent = new HotKeyEventArgs(m.LParam);
						OnHotKeyPressed(eevent);
					}
					base.WndProc(ref m);
				}

				protected override void SetVisibleCore(bool value)
				{
					base.SetVisibleCore(false);
				}
			}
		}

		public class HotKeyEventArgs : EventArgs
		{
			//randstart
			public readonly Keys Key;
			public readonly KeyModifiers Modifiers;
			//randend

			public HotKeyEventArgs(Keys keyevent, KeyModifiers modifiersevent)
			{
				//randstart
				Key = keyevent;
				Modifiers = modifiersevent;
				//randend
			}

			public HotKeyEventArgs(IntPtr hotKeyParam)
			{
				var param = (uint)hotKeyParam.ToInt64();
				Key = (Keys)((param & 0xffff0000) >> 16);
				Modifiers = (KeyModifiers)(param & 0x0000ffff);
			}
		}

		public static class Weapons
		{
			public enum FiringMode
			{
				//randstart
				Automatic = 1,
				SemiAutomatic = 2,
				//randend
			}

			//randstart
			public static List<WeaponItem> WeaponList = new List<WeaponItem>();
			public static WeaponItem CurrentWeapon;
			//randend

			public static void WeaponCycle(int forward = 1)
			{
				int currentIdx = WeaponList.FindIndex(x => x == CurrentWeapon);
				if (currentIdx != -1)
				{
					if (forward == 1)
					{
						if (currentIdx == WeaponList.Count - 1)
						{
							CurrentWeapon = WeaponList[0];
							Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
							Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
						}
						else
						{
							CurrentWeapon = WeaponList[currentIdx + 1];
							Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
							Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
						}
					}
					else
					{
						if (currentIdx == 0)
						{
							CurrentWeapon = WeaponList[WeaponList.Count - 1];
							Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
							Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
						}
						else
						{
							CurrentWeapon = WeaponList[currentIdx - 1];
							Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
							Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
						}
					}
				}
				else
				{
					Logger.Log(Logger.MessageType.ERROR, "Malfunction in Function 'WeaponCycle'");

					CurrentWeapon = WeaponList[0];
					Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
					Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
				}
			}

			private static int[][] ArrayParser(string arrayAsString)
			{
				MatchCollection allMatchResults = null;

				//randstart
				var regexObj = new Regex(@"#\{.*?\}");
				string fixedString = arrayAsString.Substring(1).Remove(arrayAsString.Length - 2);
				//randend

				allMatchResults = regexObj.Matches(fixedString);

				int[][] arrToReturn = new int[allMatchResults.Count][];

				int currentPosition = 0;
				foreach (Match m in allMatchResults)
				{
					string setFixed = m.Value.Substring(1).Remove(m.Value.Length - 2).Trim(' ');

					//randstart
					int x = Convert.ToInt32(setFixed.Split(',')[0]);
					int y = Convert.ToInt32(setFixed.Split(',')[1]);
					//randend

					int[] subArray = new[] { x, y };
					arrToReturn[currentPosition] = subArray;
					currentPosition++;
				}

				return arrToReturn;
			}

			public static void InitializeWeapons(string file, int weaponIndex = -1)
			{
				if (file != "" && File.Exists(file))
				{
					if (WeaponList.Count > 0)
					{

						WeaponList.Clear();
					}

					if (IniClass.OpenIni(file))
					{
						IniClass.Sections[] sections = IniClass.Reader.ExtractSections("WEAPON_");
						IniClass.CloseIni();

						if (sections.Length < 1)
						{
							// fucked up
							Logger.Log(Logger.MessageType.ERROR, "Malformed structure of settings file, loading default values as backup!");
							InitializeWeapons("", -1);
						}
						else
						{
							foreach (IniClass.Sections wS in sections)
							{
								WeaponItem weaponItm = new WeaponItem
								{
									//randstart
									Name = wS.SectionParameters["Name"],
									Hotkey = (Keys)int.Parse(wS.SectionParameters["HotKeyInt"]),
									ShotTiming = int.Parse(wS.SectionParameters["ShotTiming"]),
									//randend
								};
								

								if (wS.SectionParameters["Mode"] == "1")
								{
									weaponItm.Mode = FiringMode.Automatic;
								}
								else
								{
									weaponItm.Mode = FiringMode.SemiAutomatic;
								}

								//randstart
								weaponItm.ShouldSmooth = (bool)TryParse<bool>(weaponItm.Name, "ShouldSmooth", wS.SectionParameters["ShouldSmooth"].ToLower(), bool.TryParse);
								weaponItm.ShouldRandomize = (bool)TryParse<bool>(weaponItm.Name, "ShouldRandomize", wS.SectionParameters["ShouldRandomize"].ToLower(), bool.TryParse);
								weaponItm.RandomizeLowerBound = (int)TryParse<int>(weaponItm.Name, "RandomizeLowerBound", wS.SectionParameters["RandomizeLowerBound"], int.TryParse);
								weaponItm.RandomizeUpperBound = (int)TryParse<int>(weaponItm.Name, "RandomizeUpperBound", wS.SectionParameters["RandomizeUpperBound"], int.TryParse);
								weaponItm.SmoothValue = (int)TryParse<int>(weaponItm.Name, "SmoothValue", wS.SectionParameters["SmoothValue"], int.TryParse);
								weaponItm.MovementMultiplier = (double)TryParse<double>(weaponItm.Name, "MovementMultiplier", wS.SectionParameters["MovementMultiplier"], double.TryParse);
								weaponItm.RecoilTable = ArrayParser(wS.SectionParameters["RecoilTable"]);
								//randend

								WeaponList.Add(weaponItm);
							}

							if (weaponIndex != -1)
							{
								CurrentWeapon = WeaponList[weaponIndex];
							}
							else
							{
								CurrentWeapon = WeaponList[0];
							}

							foreach (var wI2 in WeaponList)
							{
								HotKeyManager.RegisteredKeys.Add(HotKeyManager.RegisterHotKey(wI2.Hotkey, ModifierKeyFromSettings));
							}

							Logger.Log(Logger.MessageType.INFO, "Successfully loaded settings from 'settings.ini'");
							Logger.Log(Logger.MessageType.INFO, "Default active weapon is " + CurrentWeapon.Name + "!");
						}
					}
				}
				else
				{
					if (WeaponList.Count > 0) WeaponList.Clear();
					var AK = new WeaponItem
					{
						RecoilTable = new[]
						{
						new[] {-36, 40}, new[] {5, 48}, new[] {-59, 48}, new[] {-49, 48}, new[] {3, 33}, new[] {20, 33}, new[] {25, 28}, new[] {45, 24}, new[] {43, 16}, new[] {32, 13}, new[] {82, 18}, new[] {8, 22}, new[] {43, 24},
						new[] {-32, 29}, new[] {-25, 33}, new[] {-40, 33}, new[] {-35, 33}, new[] {-32, 29}, new[] {-43, 22}, new[] {-42, 20}, new[] {-42, 17}, new[] {-55, 17}, new[] {-25, 17}, new[] {15, 17}, new[] {20, 20},
						new[] {35, 27}, new[] {50, 27}, new[] {62, 27}, new[] {40, 26}
					},

						//randstart
						Name = "AK",
						Hotkey = Keys.F2,
						ShotTiming = 1000 / (450 / 60),
						Mode = FiringMode.Automatic,
						ShouldRandomize = true,
						RandomizeLowerBound = 0,
						RandomizeUpperBound = 3,
						ShouldSmooth = true,
						SmoothValue = 5,
						MovementMultiplier = 3,
						//randend
					};
					var Thompson = new WeaponItem
					{
						RecoilTable = new[]
						{
						new[] {-25, 50}, new[] {0, 45}, new[] {0, 50}, new[] {0, 50}, new[] {30, 45}, new[] {0, 40}, new[] {0, 40}, new[] {0, 40}, new[] {0, 20}, new[] {-20, 20}, new[] {0, 20}, new[] {-30, 20}, new[] {0, 20},
						new[] {0, 20}, new[] {50, 20}, new[] {20, 10}, new[] {10, 10}, new[] {10, 15}
						},

						//randstart
						Name = "Thompson",
						Hotkey = Keys.F3,
						Mode = FiringMode.Automatic,
						ShotTiming = 1000 / (462 / 60),
						ShouldRandomize = true,
						RandomizeLowerBound = 0,
						RandomizeUpperBound = 3,
						ShouldSmooth = true,
						SmoothValue = 5,
						MovementMultiplier = 2,
						//randend
					};
					var M249 = new WeaponItem
					{
						RecoilTable = new[]
						{
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60},
						new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}, new[] {0, 60}
					},

						//randstart
						Name = "M249",
						Hotkey = Keys.F4,
						Mode = FiringMode.Automatic,
						ShotTiming = 1000 / (500 / 60),
						ShouldRandomize = true,
						RandomizeLowerBound = 0,
						RandomizeUpperBound = 3,
						ShouldSmooth = true,
						SmoothValue = 5,
						MovementMultiplier = 2,
						//randend
					};
					var LR300 = new WeaponItem
					{
						RecoilTable = new[]
						{
						new[] {-3, 50}, new[] {-3, 55}, new[] {-20, 75}, new[] {-5, 55}, new[] {-35, 45}, new[] {-20, 25}, new[] {-25, 25}, new[] {-5, 15}, new[] {30, 25}, new[] {30, 25},
						new[] {60, 25}, new[] {20, 35}, new[] {20, 5}, new[] {20, 5}, new[] {-30, 5}, new[] {-30, 5}, new[] {-20, 5}, new[] {-30, 5}, new[] {-30, 5}, new[] {-30, 0},
						new[] {-20, 0}, new[] {-20, 0}, new[] {-20, 0}, new[] {-30, 0}, new[] {20, 0}, new[] {30, 0}, new[] {40, 0}, new[] {50, 30}
					},

						//randstart
						Name = "LR300",
						Hotkey = Keys.F5,
						Mode = FiringMode.Automatic,
						ShotTiming = 1000 / (500 / 60),
						ShouldRandomize = true,
						RandomizeLowerBound = 0,
						RandomizeUpperBound = 3,
						ShouldSmooth = true,
						SmoothValue = 5,
						MovementMultiplier = 2,
						//randend
					};

					//randstart
					WeaponList.Add(AK);
					WeaponList.Add(Thompson);
					WeaponList.Add(M249);
					WeaponList.Add(LR300);
					//randend

					CurrentWeapon = WeaponList[0];
					Logger.Log(Logger.MessageType.INFO, "Loaded default values for weapons!");
					Logger.Log(Logger.MessageType.INFO, "Default active weapon is " + CurrentWeapon.Name + "!");
				}
			}

			public delegate bool TryParseHandler<T>(string value, out T result);
			public static T TryParse<T>(string weaponName, string property, string value, TryParseHandler<T> handler) where T : struct
			{
				if (string.IsNullOrEmpty(value))
				{
					return default(T);
				}

				T result = default(T);
				if (handler(value, out result))
				{
					return result;
				}
					
				Logger.Log(Logger.MessageType.ERROR, string.Format("Failed parsing value '{0}' to type {1} (Property: '{2}') in Weapon Config '{3}'", value, typeof(T), property, weaponName));
				MessageBox.Show(string.Format("Failed parsing value '{0}' to type {1} (Property: '{2}') in Weapon Config '{3}'", value, typeof(T), property, weaponName),
					"Failed parsing config file, exiting program!");
				Environment.Exit(-1);
				return default(T);
			}

			public static void ForceReloadSettings()
			{
				Console.Clear();
				Logger.Log(Logger.MessageType.WARNING, "A force reload of settings have been issued, performing action ...");
				try
				{
					int currentWeaponIndex = WeaponList.FindIndex(x => x == CurrentWeapon);
					InitializeWeapons(Path.Combine(Application.StartupPath, "settings.ini"), currentWeaponIndex);
				}
				catch
				{
					//randstart
					Logger.Log(Logger.MessageType.ERROR, "Command force reload failed, loading default settings ...");
					InitializeWeapons("", -1);
					//randend
				}
			}

			public static bool ParsePressedKey(Keys keyPressed)
			{
				if (WeaponList.Count < 1)
				{
					CurrentWeapon = null;

					//randstart
					Console.Title = "Current Weapon: NULL (Weapon List is empty) : " + IsEnabled.ToString().ToUpper();
					Logger.Log(Logger.MessageType.ERROR, "Something went wrong, WeaponList is empty ... ");
					//randend
					return false;
				}
				try
				{
					var lastWeapon = CurrentWeapon;
					CurrentWeapon = WeaponList.FirstOrDefault(x => x.Hotkey == keyPressed);
					if (CurrentWeapon == lastWeapon || CurrentWeapon == null) return false;
					
					//randstart
					Console.Title = "Current Weapon: " + CurrentWeapon.Name + " : " + IsEnabled.ToString().ToUpper();
					Logger.Log(Logger.MessageType.INFO, "Changed active weapon to " + CurrentWeapon.Name + "!");
					//randend

					return true;
				}
				catch
				{
					CurrentWeapon = null;

					//randstart
					Console.Title = "Current Weapon: NULL : " + IsEnabled.ToString().ToUpper();
					Logger.Log(Logger.MessageType.WARNING, "Something went wrong, Could not parse weapon ...");
					//randend
					return false;
				}
			}

			public class WeaponItem
			{
				//randstart
				public Keys Hotkey;
				public FiringMode Mode = FiringMode.Automatic;
				public double MovementMultiplier;
				public string Name;
				public int RandomizeLowerBound = 0;
				public int RandomizeUpperBound = 3;
				public int[][] RecoilTable;
				public int ShotTiming;
				public bool ShouldRandomize = true;
				public bool ShouldSmooth = true;
				public int SmoothValue = 5;
				//randend
			}
		}

		public class IniClass
		{
			public class Sections
			{
				//randstart
				public string SectionName = "";
				public Dictionary<string, string> SectionParameters = new Dictionary<string, string>();
				//randend
			}

			//randstart
			public static string[] IniRawText;
			public static List<Sections> IniParsed = new List<Sections>();
			//randend

			public static bool OpenIni(string path)
			{
				if (!File.Exists(path)) return false;

				try
				{
					IniRawText = File.ReadAllLines(path);
					Parse();
					return true;
				}
				catch
				{
					return false;
				}
			}
			public static void CloseIni()
			{
				if (IniRawText.Length > 0) IniRawText = null;
				if (IniParsed.Count > 0) IniParsed.Clear();
			}
			public static bool Parse()
			{
				try
				{
					List<int> sectionStartIndexes = new List<int>();

					for (int lineNumber = 0; lineNumber < IniRawText.Length; lineNumber++)
					{
						if (IniRawText[lineNumber].StartsWith("[") && IniRawText[lineNumber].EndsWith("]")) sectionStartIndexes.Add(lineNumber);
					}

					foreach (int sectionIdx in sectionStartIndexes)
					{
						string sectionName = IniRawText[sectionIdx].Split('[')[1].Split(']')[0];
						Sections newSection = new Sections
						{
							SectionName = sectionName.Trim(' ')
						};

						int itterator = 1;
						while (!IniRawText[sectionIdx + itterator].StartsWith("[") && (sectionIdx + itterator) <= IniRawText.Length - 1 && !string.IsNullOrWhiteSpace(IniRawText[sectionIdx + itterator]))
						{
							if (IniRawText[sectionIdx + itterator].StartsWith("//") || IniRawText[sectionIdx + itterator].StartsWith(";"))
							{
								itterator++;
								continue;
							}

							string paramName = IniRawText[sectionIdx + itterator].Split('=')[0];
							string paramValue = IniRawText[sectionIdx + itterator].Split('=')[1];

							newSection.SectionParameters.Add(paramName, paramValue);
							if (sectionIdx + itterator == IniRawText.Length - 1) break;
							if (string.IsNullOrWhiteSpace(IniRawText[sectionIdx + itterator]) || string.IsNullOrEmpty(IniRawText[sectionIdx + itterator]))
							{
								break;
							}
							itterator++;
						}

						IniParsed.Add(newSection);
					}

					return true;
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
					return false;
				}
			}

			public static class Reader
			{
				public static Sections ExtractSection(string sectionName)
				{
					return IniParsed.FirstOrDefault(x => x.SectionName.ToLower() == sectionName.ToLower());
				}
				public static Sections[] ExtractSections(string sectionsStartingWith = "#WEAPON_")
				{
					List<Sections> toReturn = new List<Sections>();
					foreach (Sections s in IniParsed)
					{
						if (s.SectionName.StartsWith(sectionsStartingWith)) toReturn.Add(s);
					}

					return toReturn.ToArray();
				}

				public static string ReadValue(string keyName, string optionalSectionName = "")
				{
					if (keyName == "") return "KEY_NAME_CANNOT_BE_BLANK";

					if (optionalSectionName == "")
					{
						foreach (Sections sect in IniParsed)
						{
							if (sect.SectionParameters.ContainsKey(keyName))
							{
								string readValue = "CANNOT_FIND_SPECIFIED_KEYNAME";
								sect.SectionParameters.TryGetValue(keyName, out readValue);
								return readValue;
							}
						}

						return "KEY_NAME_DOESNT_EXIST";
					}
					else
					{
						Sections s = IniParsed.FirstOrDefault(x => x.SectionName == optionalSectionName);
						if (s != null)
						{
							string readValue = "CANNOT_FIND_SPECIFIED_KEYNAME";
							s.SectionParameters.TryGetValue(keyName, out readValue);

							return readValue;
						}

						return "SECTION_WITH_SPECIFIED_NAME_DOES_NOT_EXIST";
					}
				}
				public static string ReadValue(Sections section, string keyName)
				{
					if (section == null) return "PASSED_SECTION_WAS_NULL";
					if (keyName == "") return "KEYNAME_CANNOT_BE_NULL";

					string readValue = "";
					if (section.SectionParameters.TryGetValue(keyName, out readValue))
					{
						return readValue;
					}
					else
					{
						return "CANNOT_FIND_VALUE_WITH_SPECIFIED_KEYNAME_IN_SECTION";
					}

				}
			}
		}

		public static class Mouse
		{

			[DllImport("#user32.dll")]
			static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

			[DllImport("#user32.dll")]
			private static extern ushort GetAsyncKeyState(int vKey);

			[DllImport("#user32.dll")]
			static extern void keybd_event(byte bVk, byte bScan, uint dwFlags,
				int dwExtraInfo);

			public static void KeyPress(byte vKeyCode = 0x22, int sleep = 95)
			{
				keybd_event(vKeyCode, 0x45, 0x1, 0);
				Thread.Sleep(sleep);
				keybd_event(vKeyCode, 0x45, 0x1 | 0x2, 0);
			}

			public static void RelativeMove(int relx, int rely)
			{
				mouse_event(0x0001, relx, rely, 0, 0);
			}

			public static bool IsKeyDown(Keys key)
			{
				return 0 != (GetAsyncKeyState((int)key) & 0x8000);
			}

		}

		public static class Drawings
		{
			//randstart
			public static SolidBrush b;
			public static Graphics g;
			public static IntPtr DrawHandle = IntPtr.Zero;
			public static bool IsDrawerInitiated;
			public static bool ShouldDrawCrosshair;
			public static int crossXLocationOffset = 0;
			public static int crossYLocationOffset = 0;
			public static bool ShouldDrawCurrentWeapon;
			//randend

			[DllImport("#User32.dll")]
			public static extern IntPtr GetDC(IntPtr hwnd);

			[DllImport("#User32.dll")]
			public static extern void ReleaseDC(IntPtr hwnd, IntPtr dc);

			public static void DrawCrosshair()
			{
				//randstart
				if (!ShouldDrawCrosshair) return;
				if (g == null) return;
				if (b == null) return;
				//randend

				// Reference Point
				//g.FillRectangle(b, new Rectangle(Screen.PrimaryScreen.Bounds.Width / 2 + crossXLocationOffset, Screen.PrimaryScreen.Bounds.Height / 2 + crossYLocationOffset, 4, 4));

				//randstart
				int crosshairLength = 15;
				int crosshairWidth = 4;
				//randend

				//randstart
				g.FillRectangle(b, centerPoint.X, centerPoint.Y - crosshairWidth - crosshairLength, crosshairWidth, crosshairLength);
				g.FillRectangle(b, centerPoint.X, centerPoint.Y + 8, crosshairWidth, crosshairLength); // Bottom
				g.FillRectangle(b, centerPoint.X + 8, centerPoint.Y, crosshairLength, crosshairWidth); // Right Side
				g.FillRectangle(b, centerPoint.X - crosshairLength - crosshairWidth, centerPoint.Y, crosshairLength, crosshairWidth); // Left side of center point
				//randend
			}

			public static void DrawCurrentWeapon()
			{
				//randstart
				if (!ShouldDrawCurrentWeapon) return;
				if (g == null) return;
				if (b == null) return;
				//randend

				g.DrawString("Current Weapon: " + Weapons.CurrentWeapon.Name + " - ENABLED: " + IsEnabled.ToString().ToUpper(), new Font("Arial", 18), b, Screen.PrimaryScreen.Bounds.Width / 2 - 1250, Screen.PrimaryScreen.Bounds.Width / 2 + 36);
			}

			public static bool TryInit()
			{
				try
				{
					//randstart
					DrawHandle = GetDC(IntPtr.Zero);
					g = Graphics.FromHdc(DrawHandle);
					b = new SolidBrush(Color.Red);
					IsDrawerInitiated = true;
					//randend

					Logger.Log(Logger.MessageType.INFO, "Drawer has been initiated successfully!");
					return true;
				}
				catch
				{
					DrawHandle = IntPtr.Zero;
					g = null;
					b = null;
					Logger.Log(Logger.MessageType.WARNING, "Drawer has failed to initiate! ");
					return false;
				}
			}

			public static bool TryDisable()
			{
				try
				{
					ReleaseDC(IntPtr.Zero, DrawHandle);

					//randstart
					if (DrawHandle != IntPtr.Zero) DrawHandle = IntPtr.Zero;
					b = null;
					g = null;
					//randend

					IsDrawerInitiated = false;

					Logger.Log(Logger.MessageType.INFO, "Drawer has deinitialized successfully!");
					return true;
				}
				catch
				{
					Logger.Log(Logger.MessageType.WARNING, "Drawer has failed to deinitialize successfully!");
					return false;
				}
			}
		}

		public static class Logger
		{
			public enum MessageType
			{
				//randstart
				INFO = 1,
				WARNING = 2,
				ERROR = 3,
				//randend
			}

			public static void Log(MessageType msgType, string msg)
			{
				//randstart
				var c = new ConsoleColor();
				var msgTypeText = "";
				//randend

				switch ((int)msgType)
				{
					case 1:
						//randstart
						c = ConsoleColor.Green;
						msgTypeText = "INFO";
						//randend
						break;
					case 2:
						//randstart
						c = ConsoleColor.Yellow;
						msgTypeText = "WARNING";
						//randend
						break;
					case 3:
						//randstart
						c = ConsoleColor.Red;
						msgTypeText = "ERROR";
						//randend
						break;
				}
				Console.ForegroundColor = c;
				Console.WriteLine("[{0}][{1}] {2}", msgTypeText, DateTime.Now.ToShortTimeString(), msg);
				Console.ResetColor();
			}
		}
	}
}