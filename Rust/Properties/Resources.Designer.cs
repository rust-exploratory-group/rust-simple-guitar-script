﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rust.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Rust.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to //randstart
        ///using System;
        ///using System.Collections;
        ///using System.Collections.Generic;
        ///using System.Drawing;
        ///using System.Linq;
        ///using System.Runtime.InteropServices;
        ///using System.Threading;
        ///using System.Text.RegularExpressions;
        ///using System.Reflection;
        ///using System.IO;
        ///using System.Runtime.Serialization.Formatters.Binary;
        ///using System.Windows.Forms;
        ///using System.Security.Principal;
        ///using System.Text;
        /////randend
        ///
        ///
        ///namespace Rust.Resources
        ///{
        ///	internal static class ScriptName
        ///	{
        ///		[Flags]
        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Rust {
            get {
                return ResourceManager.GetString("Rust", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to //randstart
        ///using System;
        ///using System.Collections.Generic;
        ///using System.Drawing;
        ///using System.Linq;
        ///using System.Runtime.InteropServices;
        ///using System.Threading;
        ///using System.Text.RegularExpressions;
        ///using System.Reflection;
        ///using System.IO;
        ///using System.Windows.Forms;
        ///using System.Security.Principal;
        /////randend
        ///
        /////randstart
        ///[assembly: AssemblyTitle(&quot;%asm_title%&quot;)]
        ///[assembly: AssemblyDescription(&quot;%asm_desc%&quot;)]
        ///[assembly: AssemblyConfiguration(&quot;&quot;)]
        ///[assembly: AssemblyCompany(&quot;%asm_comp%&quot;)]        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string RustCompiled {
            get {
                return ResourceManager.GetString("RustCompiled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to [WEAPON_AK]
        ///Name=AK
        ///HotKeyInt=113
        ///ShotTiming=134
        ///Mode=1
        ///ShouldRandomize=True
        ///RandomizeLowerBound=0
        ///RandomizeUpperBound=3
        ///ShouldSmooth=True
        ///SmoothValue=5
        ///MovementMultiplier=4
        ///RecoilTable={{-36, 40}{5, 48}{-59, 48}{-49, 48}{3, 33}{20, 33}{25, 28}{45, 24}{43, 16}{32, 13}{82, 18}{8, 22}{43, 24}{-32, 29}{-25, 33}{-40, 33}{-35, 33}{-32, 29}{-43, 22}{-42, 20}{-42, 17}{-55, 17}{-25, 17}{15, 17}{20, 20}{35, 27}{50, 27}{62, 27}{40, 26}}
        ///
        ///[WEAPON_LR300]
        ///Name=LR300
        ///HotKeyInt=116
        ///ShotTiming=120
        ///Mode=1
        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string settings {
            get {
                return ResourceManager.GetString("settings", resourceCulture);
            }
        }
    }
}
