﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.CSharp;
using Source_Randomizer;

namespace Rust
{
	internal static class Codedom
	{
		// Using Codedom
		public static int BuildExecutable(string icon, string source, string output)
		{
			Clipboard.SetText(source);
			if (source == "NULL")
			{
				MessageBox.Show(
					"Base Source is fucked up, Function ModifySource encountered an unknown variable when replacing variable names!");
				return 0;
			}

			var targetFramework = new Dictionary<string, string> {{"CompilerVersion", "v4.0"}};
			var codeProvider = new CSharpCodeProvider(targetFramework);
			var icc = codeProvider.CreateCompiler();

			var parameters = new CompilerParameters
			{
				GenerateExecutable = true,
				OutputAssembly = output
			};

			try
			{
				System.Resources.ResourceWriter writer = new System.Resources.ResourceWriter("Resources.resx");
				File.WriteAllText(Path.Combine(Application.StartupPath, "settings.ini"), Properties.Resources.settings);
				writer.AddResource("settings.ini", File.ReadAllBytes(Path.Combine(Application.StartupPath, "settings.ini")));
				writer.Generate();
				writer.Close();
				parameters.EmbeddedResources.Add("Resources.resx");
			}
			catch (Exception e)
			{
				// do nothing
			}
			

			parameters.GenerateExecutable = true;
			parameters.ReferencedAssemblies.Add("System.dll");
			parameters.ReferencedAssemblies.Add("System.Core.dll");
			parameters.ReferencedAssemblies.Add("System.Windows.Forms.dll");
			parameters.ReferencedAssemblies.Add("System.Drawing.dll");
			parameters.ReferencedAssemblies.Add("Microsoft.VisualBasic.dll");
			parameters.GenerateInMemory = false;
			parameters.WarningLevel = 0;
			parameters.TreatWarningsAsErrors = false;

			parameters.CompilerOptions = "/filealign:0x00000200 /platform:anycpu /optimize+ /debug- /d:DEBUG";

			if (icon != "" && File.Exists(icon))
				parameters.CompilerOptions += " /win32icon:" + icon;

			var results = icc.CompileAssemblyFromSource(parameters, source);

			if (results.Errors.Count > 0)
			{
				Clipboard.SetText(source);
				var i = 1;
				foreach (CompilerError CompErr in results.Errors)
				{
					MessageBox.Show("Error Message: " + CompErr.ErrorText,
						"Error Code at line " + CompErr.Line + ": " + CompErr.ErrorNumber + " [" + i + "]", MessageBoxButtons.OK,
						MessageBoxIcon.Error);
					i++;
				}

				return 0;
			}

			Clipboard.SetText(source);
			MessageBox.Show("Done!", "Compiled!", MessageBoxButtons.OK, MessageBoxIcon.Information);

			if (File.Exists(Path.Combine(Application.StartupPath, "Resources.resx"))) File.Delete(Path.Combine(Application.StartupPath, "Resources.resx"));
			if (File.Exists(Path.Combine(Application.StartupPath, "settings.ini"))) File.Delete(Path.Combine(Application.StartupPath, "settings.ini"));

			Clipboard.SetText(source);
			try
			{
				File.Delete(icon);
			}
			catch (Exception)
			{
				// fugg dat
			}

			return 1;
		}

		// If you wanna use these functions you gotta install roslyn package from nuger
		/*
		public static string[] ExtractVariableNamesFromSource(string source)
		{
			var determinedNetFramework = GetLatestNetFramework();
			var stringText = SourceText.From(source, Encoding.UTF8);
			var generatedSyntaxTree = SyntaxFactory.ParseSyntaxTree(stringText, CSharpParseOptions.Default.WithLanguageVersion(determinedNetFramework.Item2), "");

			string[] parsedVariableNames = generatedSyntaxTree.GetRoot().DescendantNodes()
				.OfType<VariableDeclaratorSyntax>().Select(v => v.Identifier.Text)
				.Concat(generatedSyntaxTree.GetRoot().DescendantNodes().OfType<ParameterSyntax>().Select(p => p.Identifier.Text))
				.ToArray();


			return parsedVariableNames;
		}
		public static string[] ExtractMethodNamesFromSource(string source)
		{
			var determinedNetFramework = GetLatestNetFramework();
			var stringText = SourceText.From(source, Encoding.UTF8);
			var generatedSyntaxTree = SyntaxFactory.ParseSyntaxTree(stringText, CSharpParseOptions.Default.WithLanguageVersion(determinedNetFramework.Item2), "");

			string[] parsedMethodNames = generatedSyntaxTree.GetRoot().DescendantNodes()
				.OfType<MethodDeclarationSyntax>().Select(f => f.Identifier.Text).ToArray();

			List<string> randomizable = new List<string>();
			foreach (var s in parsedMethodNames) // Filter out windows api function names and other functions which cannot be renamed without breaking the program
			{
				switch (s.ToLower())
				{
					case "main":
					case "unregisterhotkey":
					case "registerhotkey":
					case "setvisiblecore":
					case "wndproc":
					case "mouse_event":
					case "getasynckeystate":
					case "keybd_event":
					case "getdc":
					case "releasedc":
						break;

					default:
						randomizable.Add(s);
						break;
				}
			}

			return randomizable.ToArray();
		}
		*/
	}
}