﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Rust
{
	public class Junker
	{
		public class ClassGenerator
		{
			/*
			HOW TO USE

				* USE 'GENERATECLASS' TO GENERATE A COMPLETE CLASS WITH FUNCTIONS IN IT
				* USE 'GENERATEINLINEJUNK' TO GENERATE JUNK CODE THAT CAN BE PLACED INBETWEEN YOUR EXISTING NON-JUNK CODE
			*/
			public static string GenerateClass(int functionsInClass = 1)
			{
				if (functionsInClass < 1) functionsInClass = 1;
				var r = new Random(Guid.NewGuid().GetHashCode());
				string[] accessModifiers = {"public ", "private ", ""};
				var accessModifier = accessModifiers[r.Next(accessModifiers.Length)];
				var start = string.Format("{0}static class {1} ", accessModifier, Misc.GenerateRandomString(126)) + "{";
				var body = GenerateClassBody(functionsInClass);
				const string end = "}";
				return string.Format("{0}\n{1}\n{2}", start, body, end);
			}

			private static string GenerateClassBody(int functionCount = 3, int parameterCountMin = 3, int parameterCountMax = 11)
			{
				if (functionCount < 1) functionCount = 1;
				var body = "";
				string[] functionReturnTypes = {"int", "uint", "string", "void"};
				string[] accessModifiersFunctions = {"public ", "private ", ""};
				const string fStart = "\n{0}static {1} {2}({3})";
				var fBody = "";
				const string fEnd = "\n}\n";
				var r = new Random(Guid.NewGuid().GetHashCode());
				for (var i = 0; i < functionCount; i++)
				{
					var parameterCount = r.Next(parameterCountMin, parameterCountMax);
					var parameterString = GenerateParameters(parameterCount);
					var functionReturnType = functionReturnTypes[r.Next(functionReturnTypes.Length)];
					var start = string.Format(fStart,
						            accessModifiersFunctions[r.Next(accessModifiersFunctions.Length)],
						            functionReturnType,
						            Misc.GenerateRandomString(78, 127),
						            parameterString) + "\n{\n";
					// generate function body
					var baseVariableName = Misc.GenerateRandomString(43, 127);
					var variableNameFull = "";
					if (functionReturnType != "void")
						variableNameFull = functionReturnType + " " + baseVariableName + ";";
					else
						variableNameFull = functionReturnTypes[r.Next(functionReturnTypes.Length)] + " " + baseVariableName + ";";
					// make sure it returns something;
					switch (functionReturnType)
					{
						case "string":
							// return string
							variableNameFull = variableNameFull.Replace(";", string.Empty);
							variableNameFull += @" = """ + Misc.GenerateRandomString(64, 127) + @""";";
							fBody += "\n" + variableNameFull + "\n";
							fBody += GenerateFunctionBody(baseVariableName, "string");
							fBody += "\nreturn " + baseVariableName + ";";
							fBody += fEnd;
							break;
						case "int":
							variableNameFull = variableNameFull.Replace(";", string.Empty);
							variableNameFull += " = " + new Random(Guid.NewGuid().GetHashCode()).Next(int.MaxValue / 2, int.MaxValue) + ";";
							fBody += "\n" + variableNameFull + "\n";
							fBody += GenerateFunctionBody(baseVariableName, "int");
							fBody += "\nreturn " + baseVariableName + ";";
							fBody += fEnd;
							break;
						case "uint":
							variableNameFull = variableNameFull.Replace(";", string.Empty);
							variableNameFull +=
								" = 0x" + new Random(Guid.NewGuid().GetHashCode()).Next(int.MaxValue / 2, int.MaxValue).ToString("x") + ";";
							fBody += "\n" + variableNameFull + "\n";
							fBody += GenerateFunctionBody(baseVariableName, "uint");
							fBody += "\nreturn " + baseVariableName + ";";
							fBody += fEnd;
							break;
						case "void":
							string[] voidFunctionBody = {"int", "uint", "string"};
							fBody += GenerateFunctionBody(
								voidFunctionBody[new Random(Guid.NewGuid().GetHashCode()).Next(voidFunctionBody.Length)], baseVariableName);
							fBody += "\n return;\n";
							fBody += fEnd;
							break;
					}
					body += start + fBody;
				}
				return body;
			}

			private static string GenerateParameters(int count, bool hasDefaultValues = true)
			{
				if (count < 1) return "";
				string[] parameterTypes = {"string", "int", "uint", "char"};
				var r = new Random(Guid.NewGuid().GetHashCode());
				var parameterString = "";
				for (var i = 0; i < count; i++)
				{
					var parameterType = parameterTypes[r.Next(parameterTypes.Length)];
					var parameterDefaultValue = "";
					if (i == count - 1)
					{
						if (hasDefaultValues)
						{
							switch (parameterType)
							{
								case "string":
									// generate default value string type
									parameterDefaultValue = @"""" + Misc.GenerateRandomString(32, 83) + @"""";
									break;
								case "int":
									parameterDefaultValue = r.Next(int.MinValue, int.MaxValue).ToString();
									break;
								case "uint":
									// generate default value uint type
									parameterDefaultValue = "0x" + r.Next(int.MinValue, int.MaxValue).ToString("X");
									break;
								case "char":
									//parameterDefaultValue = "'" + Misc.GenerateRandomString(1) + "'";
									parameterDefaultValue = string.Format("'{0}'", Misc.GenerateRandomChar());
									break;
							}
							parameterString += parameterType + " " + Misc.GenerateRandomString(73, 127) + " = " + parameterDefaultValue;
						}
						else
						{
							parameterString += parameterType + " " + Misc.GenerateRandomString(64, 127);
						}
					}
					else
					{
						if (hasDefaultValues)
						{
							switch (parameterType)
							{
								case "string":
									// generate default value string type
									parameterDefaultValue = @"""" + Misc.GenerateRandomString(23, 126) + @"""";
									break;
								case "int":
									parameterDefaultValue = r.Next(int.MinValue, int.MaxValue).ToString();
									break;
								case "uint":
									// generate default value uint type
									parameterDefaultValue = "0x" + r.Next(int.MinValue, int.MaxValue).ToString("X");
									break;
								case "char":
									parameterDefaultValue = string.Format("'{0}'", Misc.GenerateRandomChar());
									break;
							}
							parameterString += parameterType + " " + Misc.GenerateRandomString(13, 87) + " = " + parameterDefaultValue +
							                   ", ";
						}
						else
						{
							parameterString += parameterType + " " + Misc.GenerateRandomString(65, 112) + ", ";
						}
					}
				}
				return parameterString;
			}

			public static string GenerateFunctionBody(string baseVariableName = "", string baseVariableType = "",
				bool isInline = false)
			{
				var functionBody = "";
				switch (baseVariableType)
				{
					case "string":
						functionBody += GenerateBodyRandom("string", baseVariableName, isInline);
						break;
					case "int":
						functionBody += GenerateBodyRandom("int", baseVariableName, isInline);
						break;
					case "uint":
						functionBody += GenerateBodyRandom("uint", baseVariableName, isInline);
						break;
				}
				if (isInline)
					switch (baseVariableType)
					{
						case "string":
							return string.Format("{0}\n{1}",
								baseVariableType + " " + baseVariableName + @" = """ + Misc.GenerateRandomString(32, 127) + @""";",
								functionBody);
						case "int":
							return string.Format("{0}\n{1}",
								baseVariableType + " " + baseVariableName + " = " +
								new Random(Guid.NewGuid().GetHashCode()).Next(int.MinValue, int.MaxValue) + ";", functionBody);
						case "uint":
							return string.Format("{0}\n{1}",
								baseVariableType + " " + baseVariableName + " = 0x" +
								new Random(Guid.NewGuid().GetHashCode()).Next(0, int.MaxValue).ToString("X") + ";", functionBody);
					}
				return functionBody;
			}

			private static string GenerateBodyRandom(string type, string baseVariableName, bool isInline = false)
			{
				var body = "";
				if (type == "string" || type == "int" || type == "uint")
				{
					var snippetInt = new List<string>(new[]
					{
						"%var%++;",
						"if(%var% == %randomnumber%) return %randomnumber%;",
						"%var%+=%randomnumber%;",
						"if (%var% - %randomnumber == (%randomNumber% - %randomNumber%)) %var% = %var% + (%randomnumber% / 4) - %randomnumber%; %var%++;",
						"%var%=%var%-(%var%-5);",
						"if(%var% % 2==0)%var%+=%randomnumber%;",
						"++%var%;",
						"%var% = %var% ^ %randomnumber%;",
						"if(%var% % 2!=0)%var%-=(%randomnumber%-2);",
						"%var%--;",
						"while(%var% < (%var% - %randomnumber%)) System.Threading.Thread.Sleep(%randomnumber%);"
					});
					var snippetString = new List<string>(new[]
					{
						@"%var% = %var%.ToLower().Replace(""%randomchar%"", ""%randomchar%"");",
						@"%var%+=""%randomchar%"";",
						"%var%=%var%;",
						"%var% = %var%.ToLower();",
						@"if(%var% != ""%randomchar%"") %var% = ""%randomchar%"";",
						@"switch(%var%) { case ""%randomchar%"": %var% = new String(%var%.ToCharArray().Reverse()); break; }", // remove if broken
						@"char[] %randomchar% = %var%.ToCharArray(); int len = %randomchar%.Length - 1; for (int i = 0; i < len; i++) { %randomchar%[i] ^= %randomchar%[len]; %randomchar%[len] ^= %randomchar%[i]; %randomchar%[i] ^= %randomchar%[len]; }", // remove if broken
						"%var% = %var%.Reverse().ToArray().ToString();",
						@"%var% = %var%.ToUpper().Replace(""%randomchar%"", ""%randomchar%"");",
						"for(int %randomchar% = 0; %randomchar% < %var%.Length; %randomchar%++) { if (%randomchar% == (%var%.Length - 1)) Debug.WriteLine(%var%); }",
						@"%var% = %var% + ""%randomchar%"";",
						"%var% = %var%.ToUpper();",
						@"if(%var% == ""%randomchar%"") %var% = ""%randomchar%"";"
					});
					var limit = 0;
					var randomChoice = "";
					switch (type)
					{
						case "string":
							limit = snippetString.Count;
							for (var i = 0; i < limit; i++)
							{
								randomChoice = snippetString[new Random().Next(snippetString.Count)];
								if (isInline && randomChoice.Contains("return"))
								{
									snippetString.Remove(randomChoice);
								}
								else
								{
									randomChoice = randomChoice.Replace("%var%", baseVariableName).Replace("%randomnumber%",
											               "0x" + new Random(Guid.NewGuid().GetHashCode()).Next(0, 8372).ToString("X"))
										               .Replace("%randomchar%", Misc.GenerateRandomString(54, 127)) + "\n";
									body += randomChoice;
									snippetString.Remove(randomChoice);
								}
							}
							break;
						case "int":
							limit = snippetInt.Count;
							for (var i = 0; i < limit; i++)
							{
								randomChoice = snippetInt[new Random().Next(snippetInt.Count)];
								if (isInline && randomChoice.Contains("return"))
								{
									snippetInt.Remove(randomChoice);
								}
								else
								{
									randomChoice = randomChoice.Replace("%var%", baseVariableName).Replace("%randomnumber%",
										               new Random(Guid.NewGuid().GetHashCode()).Next(int.MinValue, int.MaxValue / 3).ToString()) +
									               "\n";
									body += randomChoice;
									snippetInt.Remove(randomChoice);
								}
							}
							break;
						case "uint":
							limit = snippetInt.Count;
							for (var i = 0; i < limit; i++)
							{
								randomChoice = snippetInt[new Random().Next(snippetInt.Count)];
								if (isInline && randomChoice.Contains("return"))
								{
									snippetInt.Remove(randomChoice);
								}
								else
								{
									randomChoice = randomChoice.Replace("%var%", baseVariableName).Replace("%var%", baseVariableName)
										               .Replace("%randomnumber%",
											               "0x" + new Random(Guid.NewGuid().GetHashCode()).Next(0, 8372).ToString("X")) + "\n";
									body += randomChoice;
									snippetInt.Remove(randomChoice);
								}
							}
							break;
					}
				}
				return body;
			}
		}

		public static class InlineJunk
		{
			public static string GenerateInlineJunk(int junkClusterCount = 1, string junkType = "string")
			{
				if (junkClusterCount < 1) junkClusterCount = 1;
				var toReturn = "";
				switch (junkType)
				{
					case "string":
					case "int":
					case "uint":
						for (var i = 0; i < junkClusterCount; i++)
							toReturn += "\n" + ClassGenerator.GenerateFunctionBody(Misc.GenerateRandomString(64, 127), junkType, true);
						break;
					case "random":
						string[] items = {"string", "uint", "int"};
						return GenerateInlineJunk(junkClusterCount, items[new Random(Guid.NewGuid().GetHashCode()).Next(items.Length)]);
					default:
						Debug.WriteLine("Function Generate inline junk was called with an invalid type '" + junkType +
						                "', returning junk of type string instead!");
						return GenerateInlineJunk(junkClusterCount, "string");
				}
				return toReturn;
			}
		}

		public static class Misc
		{
			public static string GenerateRandomString(int minLen = 3, int maxLen = 127)
			{
				if (minLen < 3) minLen = 3;
				if (maxLen > 127) maxLen = 127;
				var coupon = new StringBuilder();
				var rng = new RNGCryptoServiceProvider();
				var rnd = new byte[1];
				var n = 0;
				var len = new Random(Guid.NewGuid().GetHashCode()).Next(minLen, maxLen);
				while (n < len)
				{
					rng.GetBytes(rnd);
					var c = (char) rnd[0];
					if ((char.IsDigit(c) || char.IsLetter(c)) && rnd[0] < 127)
					{
						++n;
						coupon.Append(rnd[0]);
					}
				}
				var allowed = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
				return allowed[new Random(Guid.NewGuid().GetHashCode()).Next(allowed.Length)] + coupon.ToString();
			}

			public static string GenerateRandomChar()
			{
				var available = "abcdefghijklmnopqrstuvwxyx".ToCharArray();
				var r = new Random(Guid.NewGuid().GetHashCode());
				return available[r.Next(available.Length)].ToString();
			}
		}
	}
}