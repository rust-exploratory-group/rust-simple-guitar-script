﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Source_Randomizer
{
	class Randomizer
	{
		/* 
		 * WHEN DONE, REMOVE ALL FUNCTIONALITY RELATING TO DISPLAYING THE RESULT TO THE USER
		 */

		public static string BaseSource = "";
		public static List<string> SplitBaseSource = new List<string>();

		public static bool OpenDocument(string document)
		{
			if (document != "")
			{
				BaseSource = document;
				SplitBaseSource = BaseSource.Split('\n').ToList();
				return true;
			}

			return false;
		}

		public static void CloseDocument()
		{
			if (BaseSource != "")
			{
				SplitBaseSource.Clear();
				BaseSource = "";
			}
		}

		public static string DocumentToString()
		{
			string doc = "";
			foreach (string s in SplitBaseSource)
			{
				doc += "\n" + s;
			}

			return doc;
		}

		public static void LineOrderRandomizer()
		{
			if (SplitBaseSource.Count < 1) return;
			Random R = new Random(Guid.NewGuid().GetHashCode());

			while (GetNextBlock(out Tuple<int, int> res))
			{
				if (res.Item1 == -1 || res.Item2 == -1) continue;

				List<string> parsed = SplitBaseSource.Skip(res.Item1).Take(res.Item2 - res.Item1 +1).ToList();
				parsed.RemoveAt(0);
				parsed.RemoveAt(parsed.Count - 1);

				parsed = parsed.OrderBy(x => R.Next()).ToList();

				if (res.Item1 == 0)
				{
					List<string> after = SplitBaseSource.Skip(res.Item2 + 1).ToList();
					parsed.AddRange(after);

					SplitBaseSource = parsed;
				}
				else
				{
					List<string> before = SplitBaseSource.Take(res.Item1).ToList();
					before.AddRange(parsed);
					before.AddRange(SplitBaseSource.Skip(res.Item2 + 1).ToList());

					SplitBaseSource = before;
				}

			}
		}

		public static bool GetNextBlock(out Tuple<int, int> results, bool stripFaultyTags = false)
		{
			//randstart
			//randend

			if (SplitBaseSource.Count < 1)
			{
				results = Tuple.Create(-1, -1);
				return false;
			}

			int startIdx = SplitBaseSource.FindIndex(x => x.Replace("\t", string.Empty).Replace("\r", string.Empty) == "//randstart");
			if (startIdx == -1)
			{
				results = Tuple.Create(-1, -1);
				return false;
			}

			for (int idx = startIdx; idx < SplitBaseSource.Count; idx++)
			{
				if (SplitBaseSource[idx].Replace("\t", string.Empty).Replace("\r", string.Empty) != "//randend")
				{
					if (SplitBaseSource[idx].Replace("\t", string.Empty).Replace("\r", string.Empty) == "//randstart" && stripFaultyTags)
					{
						// Encountered another '//randstart' tag before a '//randend' tag
						// Block should be considered faulty and tags should be stripped if stripFaultyTags is set to true
						SplitBaseSource.RemoveAt(startIdx);
					}
				}
				else
				{
					// Found one proper block
					// Return its indexes
					results = Tuple.Create(startIdx, idx);
					return true;
				}
			}

			results = Tuple.Create(-1, -1);
			return false;
		}
	}
}
